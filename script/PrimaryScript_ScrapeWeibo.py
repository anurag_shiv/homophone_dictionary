# -*- coding: utf-8 -*-
#!/usr/bin/Python2.7
#
#Author:- Anurag Shivaprasad
#Date:- October 2016
#Objective:- To fetch posts from weibo using the weibo API and store word and their Term Frequency in mongoDB across all collected posts

from WeiboPublicTimeline import WeiboPublicTimeline
from toMongo import toMongo
from MongoConnect import MongoConnect
import pandas as pd
import time
from bson.json_util import dumps
import json

stamp=time.time()
mongoconnect = MongoConnect(db = 'freeweibo')


print "SINA WEIBO FLOW BEGINNING..."
wpt = WeiboPublicTimeline()
wpt.scrapePublicTimeline(200)
wpt.extractText()
w_df = wpt.getSegmentedText(display=False,truncated = False)
w_df_tf = wpt.getTF()
w_df_tf.apply(lambda line: toMongo(line,mongoconnect,'TF',stamp),axis = 1)
print "SINA WEIBO FLOW COMPLETE"
