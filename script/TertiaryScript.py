# -*- coding: utf-8 -*-
#!/usr/bin/Python2.7
#
#Author:- Anurag Shivaprasad
#Date:- October 2016
#Objective:- To remove all keywords that are older than the set amount of time from the current date
import pandas as pd
import numpy as np
from MongoConnect import MongoConnect
import time

mongoconnect = MongoConnect(db="freeweibo")
earlierthan_30=time.time()-2592000
count = mongoconnect.deleteDocuments(collection="keywords",query="added_on < %d" % earlierthan_30)
print count
